
const page = document.querySelector(".main");
let firstNumber = setNumber(`Введіть перше число послідовності`, 10);
let secondNumber = setNumber(`Введіть друге число послідовності`, 20);
let n = setNumber(`Введіть порядковый номер числа Фібоначчі`, 7);

const result = fibonacci(firstNumber, secondNumber, n);

const str1 = `${n}-є число заданої послідовності Фібоначчі = ${result[n - 1]}`;
const str2 = `Всі числа зданної послідовності Фібоначчі: ${JSON.stringify(result)}`;

page.insertAdjacentHTML('beforeend', `<p>${str1}</p>`);
page.insertAdjacentHTML('beforeend', `<p>${str2}</p>`);


function setNumber(text, value) {
   let num = +prompt(text, value);
   while (!Number.isInteger(num)) {
      alert(`Ви ввели не корректні данні!`);
      num = +prompt("Повторіть ввід цілого числа", num);
   }
   return num;
}

function fibonacci(num1, num2, target) {
   let first = num1 + num2;
   let second = num2 + first;
   const arr = [num1, num2, first, second];
   for (let i = 4; i < target; i++) {
      const nextFib = first + second;
      arr.push(nextFib);
      first = second;
      second = nextFib;
   }
   return arr;
}